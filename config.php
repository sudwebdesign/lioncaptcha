<?php if(!defined('PLX_ROOT')) exit;
/**
* Plugin LionCaptcha
*
* @package  PLX
* @version  2.0
* @date 18/09/2019
* @author Cyril MAGUIRE
**/

if (!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
define('LIONCAPTCHA_CONFIG',PLX_ROOT.PLX_CONFIG_PATH.'plugins'.DS);?>
<?php
define('LC_COMMENT', '# File for turing test questions. Structure of the file is very simple, first
# line of a record is "--" which indicates new record (question). Second line
# is question and third line is right answer. You can add more answers to third
# separated by comma. Everything else is ignored, so you can use it as comments.
# In that case, please use something like "#" or "//" to make it clear it
# is comment. Comparing answers is case insensitive.

');
function getExt($file)
{
    $composants = explode('.',$file);
    return end($composants);
}
    $colorsOfPages = empty($plxPlugin->getParam('colorsOfPages')) ? array() : json_decode($plxPlugin->getParam('colorsOfPages'),true);
    if(!empty($_POST)) {
        $id = '';

        if (isset($_POST['newFile']) && !is_file(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.plxUtils::strCheck($_POST['newFile']).'_questions.ini')) {
            touch(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.plxUtils::strCheck($_POST['newFile']).'_questions.ini');
            touch(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.plxUtils::strCheck($_POST['newFile']).'_fake_questions.ini');
        }
        $txt = LC_COMMENT;
        if (isset($_POST['q'],$_POST['r'],$_POST['f']) && count($_POST['q']) == count($_POST['r'])) {
            if (is_file(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS. plxUtils::strCheck($_POST['f']))) {
                $i = 0;
                $hashFile = substr(plxUtils::strCheck($_POST['f']),0,2).'_hash.ini';
                file_put_contents(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.$hashFile,'');// On purge les hashs
                foreach ($_POST['q'] as $key => $value) {
                    if (trim(plxUtils::strCheck($_POST['r'][$key]) != '')) {
                        $txt .= "--\n";
                        $txt .= plxUtils::strCheck($_POST['q'][$key])."\n";
                        $txt .= plxUtils::strCheck($_POST['r'][$key])."\n";
                        file_put_contents(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.$hashFile,password_hash(plxUtils::strCheck($_POST['q'][$key]), PASSWORD_DEFAULT)."\n",FILE_APPEND);//On recrée les hashs
                    }
                }
                $txt = substr($txt,0,-1);
            }
            $fname = plxUtils::strCheck($_POST['f']);
            file_put_contents(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS. $fname,$txt);
            $id = '#trad_'.substr($fname,0,-4);
        }
        if (isset($_POST['ff'],$_POST['fake'])) {
            if (is_file(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS. plxUtils::strCheck($_POST['ff']))) {
                $fake = '';
                foreach ($_POST['fake'] as $key => $value) {
                    $fake .= plxUtils::strCheck($_POST['fake'][$key])."\n";
                }
                $fake = substr($fake,0,-1);
            }
            $fname = plxUtils::strCheck($_POST['ff']);
            file_put_contents(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS. $fname,$fake);
            $id = '#trad_'.substr($fname,0,-4);
        }
        if (isset($_POST['page'])) {
            foreach ($_POST['page'] as $key => $value) {
                $color = trim($_POST['color'][$key]);
                if ($color != '') {
                    if ($value == '') $value = 0;
                    $colorsOfPages[$value] = $color;
                }
            }
            $plxPlugin->setParam('colorsOfPages', json_encode($colorsOfPages), 'string');
        }
        if(isset($_POST['genererInput']) && isset($_POST['dir'])) {
            $plxPlugin->setParam('dir', (empty($_POST['dir']) ? 'LionCaptcha' : plxUtils::strCheck($_POST['dir'])), 'string');
            $plxPlugin->setParam('genererInput', 1, 'numeric');
            $plxPlugin->saveParams();
        }
        if(!isset($_POST['genererInput']) && isset($_POST['dir'])) {
            $plxPlugin->setParam('dir', (empty($_POST['dir']) ? 'LionCaptcha' : plxUtils::strCheck($_POST['dir'])), 'string');
            $plxPlugin->setParam('genererInput', 0, 'numeric');
            $plxPlugin->saveParams();
        }
        header('Location: parametres_plugin.php?p=LionCaptcha'.$id);
        exit;
    }
?>

<p><?php $plxPlugin->lang('L_DESCRIPTION') ?></p>
<div id="base">
<h2><?php $plxPlugin->lang('L_GENERAL_CONFIG') ?></h2>
<form action="parametres_plugin.php?p=LionCaptcha" method="post" >

    <p>
        <label><?php $plxPlugin->lang('L_DIR') ?> :
            <input type="text" name="dir" placeholder="LionCaptcha/" value="<?php echo ($plxPlugin->getParam('dir') == '' ? 'LionCaptcha/' : $plxPlugin->getParam('dir'));?>" size="20"/>
            &nbsp;<a class="hint"><span><?php echo L_HELP_SLASH_END ?><br />Ex: LionCaptcha/</span></a>
        </label>
    </p>
    <p>
        <label><?php $plxPlugin->lang('L_MAKE_INPUT') ?> :
            <input type="checkbox" name="genererInput" <?php echo ($plxPlugin->getParam('genererInput') == 1 ? 'checked="checked"' : '');?>/>
        </label>
    </p>
    <h3><?php $plxPlugin->lang('L_FONT_COLORS_OF_PAGES') ?></h3>
    <?php $i = 0;
    foreach ($colorsOfPages as $page => $color) {
        echo '<div class="inline">'.$plxPlugin->getLang('L_PAGE').' : ';
        plxUtils::printInput('pages['.$i.']',$page,'text','35-255');
        echo '&nbsp;'.$plxPlugin->getLang('L_COLOR').' : ';
        plxUtils::printInput('colors['.$i.']',$color,'text','35-255');
        echo '</div>';
        $i++;
    }
    echo '<div class="inline">'.$plxPlugin->getLang('L_PAGE').' : ';
    plxUtils::printInput('page['.$i.']','','text','35-255',false,'','url of page (return by plxUtils::getGets()');
     echo '&nbsp;'.$plxPlugin->getLang('L_COLOR').' : ';
    plxUtils::printInput('color['.$i.']','','text','35-255',false,'','black or white or 0,0,0');
    echo '</div>';?>

    <br />
    <input type="submit" name="submit" value="<?php $plxPlugin->lang('L_REC') ?>"/>
</form>
</div>
<h2><?php $plxPlugin->lang('L_FILES_CONFIG') ?></h2>
<p><?php $plxPlugin->lang('L_HELP_MAKE_FILE'); ?></p>
<?php if($plxPlugin->getParam('dir') != '' && is_dir(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir'))) {
    $trad = array();
    $fakes = array();
    $ritit = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir'), RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);
    $iter = array();
    foreach ($ritit as $splFileInfo) {
       $path = $splFileInfo->isDir()
             ? array($splFileInfo->getFilename() => array())
             : array($splFileInfo->getFilename());

       for ($depth = $ritit->getDepth() - 1; $depth >= 0; $depth--) {
           $path = array($ritit->getSubIterator($depth)->current()->getFilename() => $path);
       }
       $iter = array_merge_recursive($iter, $path);
    }
    foreach ($iter as $key => $file) {
        if ($file != 'index.html' && getExt($file) == 'ini' && substr($file,2) != '_hash.ini' && strpos($file,'fake') === false) {
            $trad[$file] = file(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.$file);
        }
        if (strpos($file,'fake') !== false) {
            $fakes[$file] = file(LIONCAPTCHA_CONFIG.$plxPlugin->getParam('dir').DS.$file);
        }
    }
    ksort($trad);
    ksort($fakes);

    foreach ($trad as $name => $lines) {
        $i = 0;
        $j = 1;
        echo '<div id="trad_'.substr($name,0,-4).'" class="trad-form">
        <h3>'.$plxPlugin->getLang('L_TITLE_MODIF_FILE').' "'.$name.'"</h3>
        <form action="parametres_plugin.php?p=LionCaptcha" method="post" accept-charset="utf-8">
        ';
        foreach ($trad[$name] as $line => $content) {
            if ($line > 7) {
                $content = trim($content);
                if ($content != '--' && $content != '') {
                    if ($i == 0) {
                        echo '<div class="inline">'.$plxPlugin->getLang('L_QUESTION').' '.str_pad($j,2,0,STR_PAD_LEFT).' : ';
                        plxUtils::printInput('q['.$line.']',$content,'text','35-255');
                        $i = $line;
                        $j++;
                    } else {
                        echo '&nbsp;'.$plxPlugin->getLang('L_ANSWER').' : ';
                        plxUtils::printInput('r['.$i.']',$content,'text','35-255');
                        echo '</div>';
                        $i=0;
                    }
                }
            }
        }
        echo '<div class="inline">'.$plxPlugin->getLang('L_QUESTION').' '.(str_pad($j++,2,0,STR_PAD_LEFT)).' : ';
        plxUtils::printInput('q[new]','','text','35-255');
        echo '&nbsp;'.$plxPlugin->getLang('L_ANSWER').' : ';
        plxUtils::printInput('r[new]','','text','35-255');
        echo '</div>
        <p>
        <input type="hidden" name="f" value="'.$name.'"/>
        <input type="submit" name="submit" value="'.$plxPlugin->getLang('L_REC').'"/>
        </p>
        </form>
        </div>';
    }
    echo'
    <h2>'.$plxPlugin->getLang('L_FAKE_QUESTIONS_FILES_CONFIG').'</h2>
    <p>'.$plxPlugin->getLang('L_HELP_MAKE_FAKE_QUESTIONS_FILE').'</p>';
    foreach ($fakes as $name => $lines) {
        $i = 0;
        $j = 1;
        echo '<div id="trad_'.substr($name,0,-4).'" class="trad-form">
        <h3>'.$plxPlugin->getLang('L_TITLE_MODIF_FILE').' "'.$name.'"</h3>
        <form action="parametres_plugin.php?p=LionCaptcha" method="post" accept-charset="utf-8">
        ';
        foreach ($fakes[$name] as $line => $content) {
            $content = trim($content);
            if ($content != '') {
                echo '<div class="inline">'.$plxPlugin->getLang('L_FAKE_QUESTION').' '.(str_pad($j++,2,0,STR_PAD_LEFT)).' : ';
                plxUtils::printInput('fake['.$line.']',$content,'text','35-255');
                echo '</div>';
            }
        }
        echo '<div class="inline">'.$plxPlugin->getLang('L_FAKE_QUESTION').' '.(str_pad($j++,2,0,STR_PAD_LEFT)).' : ';
        plxUtils::printInput('fake['.substr($name,0,2).'new]','','text','35-255');
        echo '</div>
        <p>
        <input type="hidden" name="ff" value="'.$name.'"/>
        <input type="submit" name="submit" value="'.$plxPlugin->getLang('L_REC').'"/>
        </p>
        </form>
        </div>';
    }
    echo '<form action="parametres_plugin.php?p=LionCaptcha" method="post" accept-charset="utf-8">
    <div class="inline">
    <h3>'.$plxPlugin->getLang('L_NEW_FILE').'</h3>';
    plxUtils::printSelect('newFile', plxUtils::getLangs());
    echo '</div>
    <p>
    <input type="submit" name="submit" value="'.$plxPlugin->getLang('L_REC').'"/>
    </p>
    </form>';
}