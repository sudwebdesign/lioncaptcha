<?php if(!defined('PLX_ROOT')) exit;
/**
* Plugin LionCaptcha
*
* @package PLX
* @version 2.0.2
* @date 04/03/2020
* @author Cyril MAGUIRE, Thomas Ingles
**/
define('DS',DIRECTORY_SEPARATOR);
class LionCaptcha extends plxPlugin {

	public $capcha = null;
	public $default_lang;
	private $infos = array();
	private $colorsOfPages = array();
	private $colorFont = 'black';

	public function __construct($default_lang) {
		# appel du constructeur de la classe plxPlugin (obligatoire)
		parent::__construct($default_lang);
		$this->default_lang = $default_lang;

		# limite l'acces a l'ecran de configuration du plugin
		$this->setConfigProfil(PROFIL_ADMIN);# PROFIL_ADMIN , PROFIL_MANAGER , PROFIL_MODERATOR , PROFIL_EDITOR , PROFIL_WRITER
		$this->colorsOfPages = json_decode($this->getParam('colorsOfPages'),true);

		if ($this->getParam('dir') != '') {
			#Pensez à changer également le nom du dossier s'il existe
			if (!defined('LION_CAPTCHA')) {
				define('LION_CAPTCHA',$this->getParam('dir'));
			}

			#constantes de langues
			$cs = 'L_ALT_ANTISPAM
L_PLEASE_ANSWER_QUESTION
L_ITEM
L_BELOW
L_LOOK_Q
L_IN_RED
L_PROPOSITIONS
L_IF_YOU_CAN_ANSWER
L_UNITES';# Ne pas indenter ;)

			# L_DIZAINES1 ... L_DIZAINES8
			for($i=1;$i<9;$i++){
				$cs .= PHP_EOL . 'L_DIZAINES' . $i;
			}

			# on crée les Constantes
			foreach(explode(PHP_EOL,$cs) as $c ) {
				if (!defined($c)) {
					define($c,$this->getLang($c));
				}
			}
			#Fin constantes de langues

			$this->capcha = new Capcha($default_lang,(isset($_SESSION)?$_SESSION:array()),$this->colorsOfPages);
			# Declaration d'un hook (existant ou nouveau)
			$this->addHook('plxShowCapchaQ','plxShowCapchaQ');
			$this->addHook('plxShowCapchaR', 'plxShowCapchaR');
			$this->addHook('IndexEnd', 'IndexEnd');#Méthode qui modifie la taille et le nombre maximum de caractères autorisés dans la zone de saisie du capcha
			$this->addHook('plxMotorNewCommentaire','plxMotorNewCommentaire');
			$this->addHook('plxMotorDemarrageNewCommentaire','plxMotorDemarrageNewCommentaire');
			$this->addHook('plxMotorDemarrageEnd','plxMotorDemarrageEnd');
		}
	}

	# Activation / desactivation
	public function OnActivate() {
		# code à executer à l’activation du plugin
	}
	public function OnDeactivate() {
		# code à executer à la désactivation du plugin
	}


	########################################
	# HOOKS
	########################################

	public function plxShowCapchaQ() {
		$this->capcha->actionBegin();
		$template = $this->capcha->template($this->getParam('genererInput'));
		$string =<<<END
			echo '$template';
			return true;
END;
		echo '<?php '.$string.'?>';
	}

	/**
	 * Méthode qui retourne la réponse du capcha // obsolète
	 *
	 * @return	stdio
	 * @author	Stéphane F.
	 **/
	public function plxShowCapchaR() {
		echo '<?php return true; ?>';# pour interrompre la fonction CapchaR de plxShow
	}

	/**
	 * Méthode qui modifie la taille et le nombre maximum de caractères autorisés dans la zone de saisie du capcha
	 * de plxmyCapchaImage
	 * @return	stdio
	 * @author	Stéphane F., Thomas Ingles
	 **/
	public function IndexEnd() {
		echo '<?php
			if(preg_match("/<input(?:.*?)name=[\'\"]rep[\'\"](?:.*)maxlength=([\'\"])([^\'\"]+).*>/i", $output, $m)) {
				$o = str_replace("maxlength=".$m[1].$m[2], "maxlength=".$m[1]."40", $m[0]);
				$output = str_replace($m[0], $o, $output);
			}
			if(preg_match("/<input(?:.*?)name=[\'\"]rep[\'\"](?:.*)size=([\'\"])([^\'\"]+).*>/i", $output, $m)) {
				$o = str_replace("size=".$m[1].$m[2], "size=".$m[1]."40", $m[0]);
				$output = str_replace($m[0], $o, $output);
			}
		?>';
	}

	public function plxMotorNewCommentaire() {
		echo '<?php return true; ?>';# pour interrompre la fonction
	}

	public function plxMotorDemarrageEnd() {
		$string = "
		\$capcha = \$this->plxPlugins->aPlugins['LionCaptcha'];
		if(\$this->aConf['capcha']) \$this->plxCapcha = \$capcha->capcha;
		";

		echo '<?php '.$string.'?>';
	}

	public function plxMotorDemarrageNewCommentaire() {
		$string = "
		\$artId = \$this->cible;
		\$content = plxUtils::unSlash(\$_POST);
		\$plxPlugin = \$this->plxPlugins->aPlugins['LionCaptcha'];
		if(strtolower(\$_SERVER['REQUEST_METHOD'])!= 'post' OR \$this->aConf['capcha'] AND (!isset(\$_POST['lci']) )) {
			\$retour = L_NEWCOMMENT_ERR_ANTISPAM;
		} else {

			# On vérifie que le capcha est correct
			if(\$this->aConf['capcha'] == 0 OR \$plxPlugin->capcha->actionBegin() === true) {
				if(!empty(\$content['name']) AND !empty(\$content['content'])) { # Les champs obligatoires sont remplis
					\$comment=array();
					\$comment['type'] = 'normal';
					\$comment['author'] = plxUtils::strCheck(trim(\$content['name']));
					\$comment['content'] = plxUtils::strCheck(trim(\$content['content']));
					# On vérifie le mail
					\$comment['mail'] = (plxUtils::checkMail(trim(\$content['mail'])))?trim(\$content['mail']):'';
					# On vérifie le site
					\$comment['site'] = (plxUtils::checkSite(\$content['site'])?\$content['site']:'');
					# On récupère l'adresse IP du posteur
					\$comment['ip'] = plxUtils::getIp();
					# index du commentaire
					\$idx = \$this->nextIdArtComment(\$artId);
					# Commentaire parent en cas de réponse
					if(isset(\$content['parent']) AND !empty(\$content['parent'])) {
						\$comment['parent'] = intval(\$content['parent']);
					} else {
						\$comment['parent'] = '';
					}
					# On génère le nom du fichier
					\$time = time();
					if(\$this->aConf['mod_com']) # On modère le commentaire => underscore
						\$comment['filename'] = '_'.\$artId.'.'.\$time.'-'.\$idx.'.xml';
					else # On publie le commentaire directement
						\$comment['filename'] = \$artId.'.'.\$time.'-'.\$idx.'.xml';
					# On peut créer le commentaire
					if(\$this->addCommentaire(\$comment)) { # Commentaire OK
						if(\$this->aConf['mod_com']) # En cours de modération
							\$retour = 'mod';
						else # Commentaire publie directement, on retourne son identifiant
							\$retour = 'c'.\$artId.'-'.\$idx;
					} else { # Erreur lors de la création du commentaire
						\$retour = L_NEWCOMMENT_ERR;
					}
				} else { # Erreur de remplissage des champs obligatoires
					\$retour = L_NEWCOMMENT_FIELDS_REQUIRED;
				}
			} else { # Erreur de vérification capcha
				\$retour = L_NEWCOMMENT_ERR_ANTISPAM;
			}
		}
		";
		echo '<?php '.$string.'?>';
	}

}

/**
* CAPCHA
* D'après LionWiki 3.2.9, (c) Adam Zivner, licensed under GNU/GPL v2 (Plugin Capcha)
*/
class Capcha {

	public $session;
	private $questions_dir;
	private $question_file;
	private $colorsOfPages = array();
	private $colorFont = 'black';
	private $lang;

	public function __construct($lang,$session,$colorsOfPages=array()) {
		$this->session = $session;
		$this->colorsOfPages = $colorsOfPages;
		$this->questions_dir = PLX_ROOT.PLX_CONFIG_PATH.'plugins'.DS.LION_CAPTCHA.DS;
		$this->question_file = $this->questions_dir;
		$this->lang = $lang;

		if (!is_dir($this->question_file)) {
			@mkdir($this->question_file);
		}
		$this->mkFrQuest();
		$this->mkEnQuest();

		if(file_exists($this->question_file.$lang.'_questions.ini'))
			$this->question_file .= $lang.'_questions.ini';
		else
			$this->question_file .= 'en_questions.ini';

	}

	private function mkFrQuest() {
		$txt = "# File for turing test questions. Structure of the file is very simple, first\n";
		$txt .= "# line of a record is \"--\" which indicates new record (question). Second line\n";
		$txt .= "# is question and third line is right answer. You can add more answers to third\n";
		$txt .= "# separated by comma. Everything else is ignored, so you can use it as comments.\n";
		$txt .= "# In that case, please use something like \"#\" or \"//\" to make it clear it\n";
		$txt .= "# is comment. Comparing answers is case insensitive.\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "De quelle couleur est le citron?\n";
		$txt .= "jaune\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Combien font 4 fois 4?\n";
		$txt .= "16, seize\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "chat = Tom, souris = ?\n";
		$txt .= "jerry\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "On prend la température avec un ...?\n";
		$txt .= "thermomètre, thermometre, termometre, termomètre\n";
		$txt .= "--\n";
		$txt .= "Corrigez le mot : aurtografe\n";
		$txt .= "orthographe\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "22 moins 17?\n";
		$txt .= "5, cinq\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Je pense donc je ... ?\n";
		$txt .= "suis\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Prénom d'Einstein?\n";
		$txt .= "Albert\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Qui est le frère de Mario ?\n";
		$txt .= "luiggi, luigi, louiji, luiji\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Où se trouve la Tour Eiffel ?\n";
		$txt .= "Paris";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Combien de lunes a la Terre ?\n";
		$txt .= "1, une";

		if(!file_exists($this->question_file.'fr_questions.ini')) {
			file_put_contents($this->question_file.'fr_questions.ini', $txt);
		}
		$fake = "Combien faut-il de fous pour changer une ampoule ?\nQuel est la couleur du cheval blanc d&#039;Henri IV ?\nSi jamais t&#039;es un robot, frappe dans tes mains ?\nÊtes-vous un robot ?\nSi plus je pédale moins vite, est-ce que j&#039;avance plus vite ?\nEst-ce que la Terre est plate ?\nQuel est le cosinus de la tangente au point x ?\nEst-ce que Droopy est heureux ?\nOù se trouve la statue de la Liberté ?";
		if(!file_exists($this->question_file.'fr_fake_questions.ini')) {
			file_put_contents($this->question_file.'fr_fake_questions.ini', $fake);
		}
		if(!file_exists($this->question_file.'index.html')) {
			file_put_contents($this->question_file.'index.html', '');
		}
	}
	private function mkEnQuest() {
		$txt = "# File for turing test questions. Structure of the file is very simple, first\n";
		$txt .= "# line of a record is \"--\" which indicates new record (question). Second line\n";
		$txt .= "# is question and third line is right answer. You can add more answers to third\n";
		$txt .= "# separated by comma. Everything else is ignored, so you can use it as comments.\n";
		$txt .= "# In that case, please use something like \"#\" or \"//\" to make it clear it\n";
		$txt .= "# is comment. Comparing answers is case insensitive.\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "What color is lemon?\n";
		$txt .= "Yellow\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "How much is 4 times 4?\n";
		$txt .= "16, sixteen\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "cat - Tom, mouse -\n";
		$txt .= "Jerry\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Shortcut of World War 2?\n";
		$txt .= "WW2, WWII\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Correct spelling: univrsity\n";
		$txt .= "University\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Difference between 22 and 17?\n";
		$txt .= "5, five\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "I think, therefore I...\n";
		$txt .= "am\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "First name of Einstein?\n";
		$txt .= "Albert\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "How many moons has the Earth?\n";
		$txt .= "1, one\n";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Name of partner of Eve of Eden?\n";
		$txt .= "Adam";
		$txt .= "\n";
		$txt .= "--\n";
		$txt .= "Where is Eiffel tower?\n";
		$txt .= "Paris";

		if(!file_exists($this->question_file.'en_questions.ini')) {
			file_put_contents($this->question_file.'en_questions.ini', $txt);
		}
		$fake = "Are you a robot ?\nWhat is the color of Henri IV white horse ?\nIs Droopy happy ?\nWhat is the cosinus for x ?\nHow many fools does it take to change a light bulb ?\nIf you are a robot, can you clap your hands ?\nIs the earth flat ?\nIf more the slower I pedal, do I move faster ?\nWhere is the Statute of Liberty ?";
		if(!file_exists($this->question_file.'en_fake_questions.ini')) {
			file_put_contents($this->question_file.'en_fake_questions.ini', $fake);
		}
	}
	public function actionBegin() {

		$page = plxUtils::getGets();
		if ($page === false) $page = 0;
		if (array_key_exists($page, $this->colorsOfPages)) {
			$this->colorFont = $this->colorsOfPages[$page];
		}

		if(isset($_REQUEST['lci'])) {
			return $this->checkCapcha();
		}
	}

	/*
	 * Functions return number of questions in question file. Method is very simple, it just counts
	 * number of occurence of "--" at the begining of the line.
	 */

	private function questionCount() {
		$count = 0;
		$q = fopen($this->question_file, 'r');

		if(!$q) {
			return 'Error';
		}

		while($line = fgets($q))
			if(!strcmp(trim($line), '--'))
				$count++;
		fclose($q);
		return $count;
	}

	/*
	 * Function returns $line. line of $i. question. Convention is that first line is question and
	 * second line is answer(s). Numbering is Pascal-like, that means that getQuestion(1, 1) returns 1. line of 1. question.
	 */

	private function getQuestion($i, $line) {
		$count = 0;
		$q = fopen($this->question_file, 'r');
		if(!$q) {
			//return 0; // Oops
			return 'Error';
		}
		$str = '';

		while($l = fgets($q)) {
			if(!strcmp(trim($l), '--')) {
				$count++;
				if($count == $i) {
					for($k = 0, $str = ''; $k < $line && $str = fgets($q); $k++);
					break;
				}
			}
		}
		fclose($q);
		return str_replace("'","\'",$str);
	}

	private function getQuestions() {
		$f = file($this->question_file);
		$questions = array();
		$i = 1;
		foreach ($f as $key => $value) {
			if (trim($value) == '--' && isset($f[($key+1)]) && trim($f[($key+1)]) != '') {
				$questions[$i] = trim($f[($key+1)]);
				$i++;
			}
		}
		return $questions;
	}

	private function checkCapcha() {
		$question_id = $_REQUEST['lci'];
		$answer = trim($_REQUEST['rep']);
		// if(empty($question_id) || empty($answer) || !is_numeric($question_id))
		// 	return true;
		$question_id = $this->validate_id($question_id);
		$right_answers = explode(',', $this->getQuestion($question_id, 2));

		$equals = false;

		foreach($right_answers as $a) {
			if(!strcasecmp(trim($a), $answer)) {
				$equals = true;
				break;
			}
		}
		return $equals;
	}

	private function getFakeQuestions() {
		if(file_exists($this->questions_dir.$this->lang.'_fake_questions.ini'))
			return file($this->questions_dir.$this->lang.'_fake_questions.ini');
		else
			return file($this->questions_dir.'en_fake_questions.ini');
	}

	private function validate_id($hash) {
		$hashes = $this->getQuestions();
		foreach ($hashes as $key => $value) {
			if (password_verify($key.$value, substr($hash,32))) {
				return $key;
			}
		}
		return false;
	}

	public function template($genererInput=0) {
		$question_count = $this->questionCount();
		$question_id = rand(1, $question_count);
		$question_text = trim($this->getQuestion($question_id, 1));
		$fakes = $this->getFakeQuestions();
		$max = 50;
		$nb = count($fakes);
		$questions = array();
		if ($nb > $max) {
			$items = array_rand($fakes,$max);
			foreach ($items as $key => $value) {
				$questions[] = $fakes[$value];
			}
		} else {
			$questions = $fakes;
		}
		$numbers = range(0, $nb);
		shuffle($numbers);
		$pos = array_rand($numbers,1);
		$select = array(null=>L_PROPOSITIONS);
		$i = 0;
		$alreadyUsed = array();
		foreach ($numbers as $key => $value) {
			$i = md5(rand(0,200));
			while(in_array($i, $alreadyUsed)) {
				$i = md5(rand(0,200));
			}
			$alreadyUsed[] = $i;
			if ($value == $pos) {
				$p = $this->rand($pos);
				$select[$i] = L_ITEM . $p . ' : ' . $question_text;
			} elseif (isset($questions[$value])) {
				$select[$i] = L_ITEM . $this->rand($pos) . ' : ' . trim($questions[$value]);
			}
		}
		$img = @$this->multilineimage($p);

		ob_start();
		plxUtils::printSelect('aQ',$select);
		$select = ob_get_clean();

		$html = '<span id="lcq">' . $img . '</span>';
		$html .= $select;
		$html .= '<input type="hidden" id="lci" name="lci" value="'.md5(rand(0,150)).password_hash($question_id.$question_text, PASSWORD_DEFAULT).'" />';
		if ($genererInput == 1) {
			$html .= '<input type="text" id="lcipt" name="lcr" class="input input-success success" value="" autocomplete="off" />';
		}
		return $html;
	}

	public function rand($pos) {#retourne un numéro aléatoire a deux chiffres, sauf celui de $pos
			$random = rand(1,99);
			if ($random == $pos OR !$random) {# Si même numéro que $pos (question rééle) ou vide.
				$random = $this->rand($pos);# On recommence
			}
		return str_pad($random,2,0,STR_PAD_LEFT);
	}

	private function randPos($pos) {
		$nb = rand(5,9);
		$aPos = array();
		for ($i=0; $i < $nb; $i++) {
			$aPos[] = str_pad($this->rand($pos),2,0,STR_PAD_LEFT);
		}
		$aPos[] = str_pad($pos,2,0,STR_PAD_LEFT);
		shuffle($aPos);
		$r = '';
		foreach ($aPos as $key => $value) {
			$r .= $value.' ';
		}
		return $r;
	}

	private function convertInText($pos) {
		$nombres = array();
		if ($this->lang == 'fr') {
			$unites = explode(',',L_UNITES);
			$dizaines = explode(',', L_DIZAINES1);
			$nombres = array_merge($unites,$dizaines);
			$vingt = array();
			$trente = array();
			$quarante = array();
			$cinquante = array();
			foreach ($unites as $key => $value) {
				if ($key == 0) {
					$vingt[] = L_DIZAINES2;
					$trente[] = L_DIZAINES3;
					$quarante[] = L_DIZAINES4;
					$cinquante[] = L_DIZAINES5;
					$soixante[] = L_DIZAINES6;
					$septante[] = L_DIZAINES6.'-'.$dizaines[$key];
					$quatrevingt[] = L_DIZAINES8;
					$quatrevingtdix[] = L_DIZAINES8.'-'.$dizaines[$key];
				} elseif ($key == 1) {
					$vingt[] = L_DIZAINES2.' et '.$value;
					$trente[] = L_DIZAINES3.' et '.$value;
					$quarante[] = L_DIZAINES4.' et '.$value;
					$cinquante[] = L_DIZAINES5.' et '.$value;
					$soixante[] = L_DIZAINES6.' et '.$value;
					$septante[] = L_DIZAINES6.' et '.$dizaines[$key];
					$quatrevingt[] = L_DIZAINES8.'-'.$value;
					$quatrevingtdix[] = L_DIZAINES8.'-'.$dizaines[$key];
				} else {
					$vingt[] = L_DIZAINES2.'-'.$value;
					$trente[] = L_DIZAINES3.'-'.$value;
					$quarante[] = L_DIZAINES4.'-'.$value;
					$cinquante[] = L_DIZAINES5.'-'.$value;
					$soixante[] = L_DIZAINES6.'-'.$value;
					$septante[] = L_DIZAINES6.'-'.$dizaines[$key];
					$quatrevingt[] = L_DIZAINES8.'-'.$value;
					$quatrevingtdix[] = L_DIZAINES8.'-'.$dizaines[$key];
				}
			}
			$nombres = array_merge($nombres,$vingt);
			$nombres = array_merge($nombres,$trente);
			$nombres = array_merge($nombres,$quarante);
			$nombres = array_merge($nombres,$cinquante);
			$nombres = array_merge($nombres,$soixante);
			$nombres = array_merge($nombres,$septante);
			$nombres = array_merge($nombres,$quatrevingt);
			$nombres = array_merge($nombres,$quatrevingtdix);
		}
		if (isset($nombres[$pos])) {
			return $nombres[$pos];
		} else {
			return $pos;
		}
	}

	private function multilineimage($pos) {

		$item = $this->convertInText($pos);

		$trad = explode(';',$this->removeAccents(L_PLEASE_ANSWER_QUESTION));
		$quest = array_rand($trad,1);
		$string = $trad[$quest]."\n".L_BELOW.' '.L_IN_RED."\n".$this->randPos($pos);

		// Probably not the best way of handling newlines, but bar OS9, doesn't really cause a problem
		$string = str_replace("\r","",$string);
		$string = explode("\n",$string);

		$maxlen = 0;
		foreach ($string as $str){
			if (strlen($str) > $maxlen){
				$maxlen = strlen($str);
			}
		}

		// Set font size
		$font_size = 4;

		// Create image width dependant on width of the string
		$width  = imagefontwidth($font_size)*$maxlen;
		// Set height to that of the font
		$height = (imagefontheight($font_size) * count($string))+(2*imagefontheight($font_size));
		// Create the image pallette
		$img = imagecreate($width,$height);
		//	Transparent background
		$background_color = imagecolorallocatealpha ($img, 255, 255, 255, 127);
		// font color
		switch ($this->colorFont) {
			case 'black':
				$text_color = imagecolorallocate($img, 0, 0,0);//black text
				break;
			case 'white':
				$text_color = imagecolorallocate($img, 255, 255,255);//white text
				break;
			default:
				$rgb = array();
				if (strpos($this->colorFont,',') !== false) {
					$rgb = explode(',',$this->colorFont);
				}
				if (count($rgb) != 3 ) {
					$rgb[0] = isset($rgb[0]) ? $rbg[0] : 0;
					$rgb[1] = isset($rgb[1]) ? $rbg[1] : 0;
					$rgb[2] = isset($rgb[2]) ? $rbg[2] : 0;
				}
				$text_color = imagecolorallocate ($img, $rgb[0], $rgb[1], $rgb[2]);
				break;
		}
		$tc = $text_color;

		$ypos = 0;

		foreach ($string as $str){

			$len = strlen($str);
			for($i=0;$i<$len;$i++){

				if (strpos(str_pad($pos,2,0,STR_PAD_LEFT),substr($str,0,(strlen($pos)))) !== false ){
					$text_color = imagecolorallocate($img, 242,11,51);//orange text
					$pos = substr($pos,1);
				} else {
					$text_color = $tc;
				}
				// Position of the character horizontally
				$xpos = $i * imagefontwidth($font_size);
				// Position of the character vertically
				$Y = $ypos;
				// Draw character
				imagechar($img, $font_size, $xpos, $Y, $str, $text_color);
				// Remove character from string
				$str = substr($str, 1);
			}
			$ypos = $ypos + imagefontheight($font_size);
		}

		ob_start();
		imagepng($img);
		$imgbinary = ob_get_contents();
		ob_end_clean();
		imagedestroy($img);

		return '<img src="data:image/png;base64,'. base64_encode($imgbinary).'" alt="'.L_LOOK_Q.$item.'. '.L_IF_YOU_CAN_ANSWER.'"/><br/>';
	}

	private static function removeAccents($str,$charset='utf-8') {

		$str = htmlentities($str, ENT_NOQUOTES, $charset);
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ','&amp;#039;');
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o','\'');
		$str = str_replace($a, $b, $str);
		$str = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml|uro)\;#', '\1', $str);
		$str = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $str); # pour les ligatures e.g. '&oelig;'
		$str = preg_replace('#\&[^;]+\;#', '', $str); # supprime les autres caractères
		$str = implode(' ',str_split($str));
		return $str;
	}

}
