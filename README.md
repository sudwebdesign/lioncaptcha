# LionCaptcha pour [PluXml](https://pluxml.org)

Plugin anti-spams, dérivé de Lion Wiki.

Le but est de créer vos propres questions/réponses afin qu'elles ne puissent pas être indexées par des robots et de créer également vos propres fausses questions, auxquelles vous n'aurez pas forcément de réponse mais qui permettront de masquer les vraies questions.
Le plugin est livré avec des exemples mais il est conseillé de ne pas les conserver.

#### Note importante : une fois désarchivé (unzip). Veuillez renommer le dossier en LionCaptcha (avec L & C en majuscule) afin qu'il soit visible par PluXml.